package id.sejahtera.model;

public class Transaksi {

    int saldo, total_pinjaman, cicilan, nominal_sudah_bayar, nominal_belum_bayar;
    String status_pinjaman;

    public Transaksi(int saldo, int total_pinjaman, int cicilan, int nominal_sudah_bayar, int nominal_belum_bayar, String status_pinjaman) {
        this.saldo = saldo;
        this.total_pinjaman = total_pinjaman;
        this.cicilan = cicilan;
        this.nominal_sudah_bayar = nominal_sudah_bayar;
        this.nominal_belum_bayar = nominal_belum_bayar;
        this.status_pinjaman = status_pinjaman;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public int getTotal_pinjaman() {
        return total_pinjaman;
    }

    public void setTotal_pinjaman(int total_pinjaman) {
        this.total_pinjaman = total_pinjaman;
    }

    public int getCicilan() {
        return cicilan;
    }

    public void setCicilan(int cicilan) {
        this.cicilan = cicilan;
    }

    public int getNominal_sudah_bayar() {
        return nominal_sudah_bayar;
    }

    public void setNominal_sudah_bayar(int nominal_sudah_bayar) {
        this.nominal_sudah_bayar = nominal_sudah_bayar;
    }

    public int getNominal_belum_bayar() {
        return nominal_belum_bayar;
    }

    public void setNominal_belum_bayar(int nominal_belum_bayar) {
        this.nominal_belum_bayar = nominal_belum_bayar;
    }

    public String getStatus_pinjaman() {
        return status_pinjaman;
    }

    public void setStatus_pinjaman(String status_pinjaman) {
        this.status_pinjaman = status_pinjaman;
    }
}
