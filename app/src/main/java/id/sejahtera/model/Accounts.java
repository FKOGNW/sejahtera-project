package id.sejahtera.model;

public class Accounts {
    private int id, saldo, nominal_sudah_bayar, nominal_belum_bayar, cicilan;
    private String username, nama_lengkap, email, no_telepon, kata_sandi, konfirmasi_kata_sandi, nama_bisnis, status_pinjaman, alamat;

    public Accounts(){}

    public Accounts(int id, int saldo, int nominal_sudah_bayar, int nominal_belum_bayar, int cicilan, String username, String nama_lengkap, String email, String no_telepon, String kata_sandi, String konfirmasi_kata_sandi, String nama_bisnis, String status_pinjaman, String alamat) {
        this.id = id;
        this.saldo = saldo;
        this.nominal_sudah_bayar = nominal_sudah_bayar;
        this.nominal_belum_bayar = nominal_belum_bayar;
        this.cicilan = cicilan;
        this.username = username;
        this.nama_lengkap = nama_lengkap;
        this.email = email;
        this.no_telepon = no_telepon;
        this.kata_sandi = kata_sandi;
        this.konfirmasi_kata_sandi = konfirmasi_kata_sandi;
        this.nama_bisnis = nama_bisnis;
        this.status_pinjaman = status_pinjaman;
        this.alamat = alamat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNo_telepon() {
        return no_telepon;
    }

    public void setNo_telepon(String no_telepon) {
        this.no_telepon = no_telepon;
    }

    public String getKata_sandi() {
        return kata_sandi;
    }

    public void setKata_sandi(String kata_sandi) {
        this.kata_sandi = kata_sandi;
    }

    public String getKonfirmasi_kata_sandi() {
        return konfirmasi_kata_sandi;
    }

    public void setKonfirmasi_kata_sandi(String konfirmasi_kata_sandi) {
        this.konfirmasi_kata_sandi = konfirmasi_kata_sandi;
    }

    public String getNama_bisnis() {
        return nama_bisnis;
    }

    public void setNama_bisnis(String nama_bisnis) {
        this.nama_bisnis = nama_bisnis;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getStatus_pinjaman() {
        return status_pinjaman;
    }

    public void setStatus_pinjaman(String status_pinjaman) {
        this.status_pinjaman = status_pinjaman;
    }

    public int getNominal_sudah_bayar() {
        return nominal_sudah_bayar;
    }

    public void setNominal_sudah_bayar(int nominal_sudah_bayar) {
        this.nominal_sudah_bayar = nominal_sudah_bayar;
    }

    public int getNominal_belum_bayar() {
        return nominal_belum_bayar;
    }

    public void setNominal_belum_bayar(int nominal_belum_bayar) {
        this.nominal_belum_bayar = nominal_belum_bayar;
    }

    public int getCicilan() {
        return cicilan;
    }

    public void setCicilan(int cicilan) {
        this.cicilan = cicilan;
    }
}
