package id.sejahtera.model;

public class Payment {
    int nominal_sudah_bayar, nominal_belum_bayar;

    public Payment(int nominal_sudah_bayar, int nominal_belum_bayar) {
        this.nominal_sudah_bayar = nominal_sudah_bayar;
        this.nominal_belum_bayar = nominal_belum_bayar;
    }

    public int getNominal_sudah_bayar() {
        return nominal_sudah_bayar;
    }

    public void setNominal_sudah_bayar(int nominal_sudah_bayar) {
        this.nominal_sudah_bayar = nominal_sudah_bayar;
    }

    public int getNominal_belum_bayar() {
        return nominal_belum_bayar;
    }

    public void setNominal_belum_bayar(int nominal_belum_bayar) {
        this.nominal_belum_bayar = nominal_belum_bayar;
    }
}
