package id.sejahtera.model;

public class Business {
    String nama_bisnis, kategori, alamat, deskripsi;
    int saldo;

    public Business(String nama_bisnis, String kategori, String alamat, String deskripsi, int saldo) {
        this.nama_bisnis = nama_bisnis;
        this.kategori = kategori;
        this.alamat = alamat;
        this.deskripsi = deskripsi;
        this.saldo = saldo;
    }

    public String getNama_bisnis() {
        return nama_bisnis;
    }

    public void setNama_bisnis(String nama_bisnis) {
        this.nama_bisnis = nama_bisnis;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
