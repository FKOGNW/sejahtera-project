package id.sejahtera.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.sejahtera.JSONResponse;
import id.sejahtera.R;
import id.sejahtera.activity.MainActivity;
import id.sejahtera.activity.PostActivity;
import id.sejahtera.adapter.ForumAdapter;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Accounts;
import id.sejahtera.model.Posting;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForumFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    FloatingActionButton fab;

    private RecyclerView recyclerView;
    private List<Posting> data;
    private ForumAdapter adapter;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    SwipeRefreshLayout mSwipeRefreshLayout;

    public ForumFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static ForumFragment newInstance() {
        ForumFragment fragment = new ForumFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_forum, container, false);

        fab= v.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PostActivity.class);
                startActivity(intent);
            }
        });

        /*create progress dialog*/
//        pDialog= new ProgressDialog(getActivity());
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Sedang menyambungkan...");
//        pDialog.show();

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primaryColor,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        recyclerView = v.findViewById(R.id.rcForum);

        data= new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        adapter= new ForumAdapter(getActivity().getApplicationContext(),data);
        recyclerView.setAdapter(adapter);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadData();
            }
        });


        return v;
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    private  void loadData(){
//        clear recycler view
        data.clear();
        adapter.notifyDataSetChanged();

        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);

        // Retrofit for GET User Info

        // Initialize the Retrofit Interface
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<List<Posting>> call = apiService.getPost();
        call.enqueue(new Callback<List<Posting>>() {
            @Override
            public void onResponse(Call<List<Posting>> call, Response<List<Posting>> response) {
//                pDialog.dismiss();
//                data= response.body();

                if (response.body() != null) {
                    data.addAll(response.body());
                }
                adapter.notifyDataSetChanged();     //always remember to notify the adapter

//                adapter = new ForumAdapter(container.getContext(),data);
//                recyclerView.setAdapter(adapter);
//                recyclerView.setHasFixedSize(true);
//                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//                recyclerView.setLayoutManager(layoutManager);
//                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
//
                Log.d("FK", "response:"+ data);
//                adapter.notifyDataSetChanged();
//                Toast.makeText(getContext(), "success response"+data, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Posting>> call, Throwable t) {
//                pDialog.dismiss();
                Log.d("Error",t.getMessage());
                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);

                /*cretae alert dialog*/
                alertDialog= new AlertDialog.Builder(getContext()).create();

                // Setting Dialog Title
                alertDialog.setTitle("Koneksi anda bermasalah");

                // Showing Alert Message
                alertDialog.show();
//                Toast.makeText(getContext(), "fail response"+data, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
