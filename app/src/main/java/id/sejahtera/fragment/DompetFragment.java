package id.sejahtera.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import id.sejahtera.R;
import id.sejahtera.activity.AddBusinessActivity;
import id.sejahtera.activity.AddSaldoActivity;
import id.sejahtera.activity.EditBusinessActivity;
import id.sejahtera.activity.MainActivity;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;


public class DompetFragment extends Fragment {

    public DompetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public static DompetFragment newInstance() {
        DompetFragment fragment = new DompetFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_dompet, container, false);

        /**
         * session management
         * */
        SharedPreferences session = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        /*set name text*/
        TextView tvBisnis= v.findViewById(R.id.tvNamaBisnis);
        TextView tvSaldo= v.findViewById(R.id.tvSaldo);
        TextView tvSudahbayar= v.findViewById(R.id.tvSudahBayar);
        TextView tvBelumbayar= v.findViewById(R.id.tvBelumBayar);
        TextView tvStatus= v.findViewById(R.id.tvStatus);
        TextView tvAlamat= v.findViewById(R.id.tvAlamat);

        final String tempBisnis= session.getString("Bisnis", "");
        String tempSaldo= String.valueOf(session.getInt("Saldo", 0));
        String tempSudahBayar= String.valueOf(session.getInt("Sudah bayar", 0));
        String tempBelumBayar= String.valueOf(session.getInt("Belum bayar", 0));
        String tempStatus= session.getString("Status", "");
        String tempAlamat= session.getString("Alamat", "");

        Button btAjukan= v.findViewById(R.id.btAjukan);

        if (tempBisnis.trim().length()>0){
            tvBisnis.setText(tempBisnis);
            tvAlamat.setText(tempAlamat);
        }else{
            tvBisnis.setText("Bisnis anda belum terdaftar");
        }


        tvStatus.setText(tempStatus);


        if (tempStatus.equals("Sedang mengajukan")){
            btAjukan.setVisibility(View.INVISIBLE);
            tvStatus.setTextColor(Color.parseColor("#FF0000"));
        }else if(tempStatus.equals("Terverifikasi")){
            tvStatus.setTextColor(Color.parseColor("#009966"));
            tvSudahbayar.setText("Rp."+ tempSudahBayar);
            tvBelumbayar.setText("Rp."+ tempBelumBayar);
            tvSaldo.setText("Rp. "+tempSaldo);
            btAjukan.setVisibility(View.INVISIBLE);
        }

        btAjukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tempBisnis.equals("")){
                    AlertDialog.Builder alertDialog= new AlertDialog.Builder(getContext());

                    // Setting Dialog Title
                    alertDialog.setTitle("Anda belum mendaftarkan bisnis anda!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Ingin mendaftarkan bisnis anda?");

                    alertDialog.setPositiveButton("YA",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    startActivity(new Intent(getContext(), AddBusinessActivity.class));
                                }
                            });

                    alertDialog.setNegativeButton("TIDAK",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });


                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    startActivity(new Intent(getContext(), AddSaldoActivity.class));
                }
            }
        });


        return v;

    }

}
