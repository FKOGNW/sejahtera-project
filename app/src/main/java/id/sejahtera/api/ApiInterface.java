package id.sejahtera.api;

import java.util.List;

import id.sejahtera.model.Accounts;
import id.sejahtera.model.Business;
import id.sejahtera.model.Payment;
import id.sejahtera.model.Posting;
import id.sejahtera.model.Transaksi;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ApiInterface {
    /**
     * For login and register
     * */
    @GET("accounts")
    Call<List<Accounts>> getUserLogin(@Query("username") String username, @Query("kata_sandi") String kata_sandi);

    @FormUrlEncoded
    @POST("accounts")
    Call<Accounts> postUser(@Field("username") String username, @Field("nama_lengkap") String nama_lengkap,
                            @Field("email") String email, @Field("no_telepon") String no_telepon,
                            @Field("kata_sandi") String kata_sandi, @Field("konfirmasi_kata_sandi") String konfirmasi_kata_sandi,
                            @Field("nama_bisnis") String nama_bisnis);

    /**
     * For update profile*/
    @GET("accounts")
    Call<List<Accounts>> getUserProfile(@Query("username") String username, @Query("kata_sandi") String kata_sandi);

    @FormUrlEncoded
    @PUT("accounts")
    Call<Accounts> putUserProfile(@Field("username") String username, @Field("nama_lengkap") String nama_lengkap,
                                  @Field("email") String email, @Field("no_telepon") String no_telepon,
                                  @Field("kata_sandi") String kata_sandi,
                                  @Field("konfimasi_kata_sandi") String konfirmasi_kata_sandi);

    /**
     * For business*/
    @FormUrlEncoded
    @PUT("business")
    Call<Business> putBusiness(@Field("username") String username,@Field("nama_bisnis") String nama_bisnis, @Field("kategori") String kategori,
                                @Field("alamat") String alamat, @Field("deskripsi") String deskripsi);

    @GET("business")
    Call<List<Business>> getUserBusiness(@Query("username") String username, @Query("kata_sandi") String kata_sandi);


    /**
     * For transaksi*/
    @GET("transaksi")
    Call<List<Transaksi>> getUserTransaksi(@Query("username") String username, @Query("kata_sandi") String kata_sandi);

    @FormUrlEncoded
    @PUT("transaksi")
    Call<Transaksi> putTransaksi(@Field("username") String username, @Field("saldo") int saldo,
                                 @Field("status_pinjaman") String status_pinjaman, @Field("total_pinjaman") int total_pinjaman,
                                 @Field("cicilan") int cicilan, @Field("nominal_belum_bayar") int nominal_belum_bayar);


    /**
     * For payment*/
    @FormUrlEncoded
    @PUT("payment")
    Call<Payment> putPayment(@Field("username") String username, @Field("nominal_sudah_bayar") int nominal_sudah_bayar,
                             @Field("nominal_belum_bayar") int nominal_belum_bayar);

    /**
     * For posting*/
    @FormUrlEncoded
    @POST("posting")
    Call<Posting> post(@Field("author") String author, @Field("title") String title,
                       @Field("post") String post, @Field("date") String date);

    @GET("posting")
    Call<List<Posting>> getPost();

}
