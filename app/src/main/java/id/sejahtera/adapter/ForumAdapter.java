package id.sejahtera.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.sejahtera.R;
import id.sejahtera.activity.MainActivity;
import id.sejahtera.activity.PostActivity;
import id.sejahtera.activity.PostingActivity;
import id.sejahtera.model.Posting;

public class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.ViewHolder>{

    private List<Posting> posting;
    Context context;
    private LayoutInflater mInflater;

//    public ForumAdapter(ArrayList<Posting> posting) {
//        this.posting = posting;
//        this.context = context;
//    }

    public ForumAdapter(Context applicationContext, List<Posting> data) {
        this.posting = data;
        this.context = applicationContext;
        this.mInflater = LayoutInflater.from(context);
    }

//    public void setPostingList(List<Posting> posting) {
//
//    }

    @NonNull
    @Override
    public ForumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_forum, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.id = posting.get(position).getId();
        holder.judul.setText(posting.get(position).getTitle());
        holder.post.setText(posting.get(position).getPost());
        holder.penulis.setText(posting.get(position).getAuthor());
        holder.waktu.setText(posting.get(position).getDate());

//        holder.posting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                /*error*/
//            }
//        });

    }


    @Override
    public int getItemCount() {
        if(posting != null){
            return posting.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private int id;
        private TextView judul, post, penulis, waktu;
        FrameLayout posting;
        public ViewHolder(View itemView) {
            super(itemView);

            posting= itemView.findViewById(R.id.flForum);

            judul= itemView.findViewById(R.id.tvTitle);
            post= itemView.findViewById(R.id.tvPost);
            penulis=itemView.findViewById(R.id.tvAuthor);
            waktu= itemView.findViewById(R.id.tvDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent in = new Intent(context, PostingActivity.class);
////                    in.putExtra("id", Integer.toString(id));
//                    context.startActivity(in);


                    Toast.makeText(context, "You Clicked : "+Integer.toString(id), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
