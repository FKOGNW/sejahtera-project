package id.sejahtera.intro;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import id.sejahtera.activity.MainActivity;
import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG= "Fk";
    SharedPreferences session;
    SharedPreferences.Editor editor;

    private Button btLogin;
    private EditText etUsername;
    private EditText etPassword;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    private String username, password;
    private List<Accounts> account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btLogin= findViewById(R.id.btLogin);
        etUsername= findViewById(R.id.etUsername);
        etPassword= findViewById(R.id.etPassword);

        /*session login*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setMessage("Sedang dalam proses...");
        pDialog.setCancelable(false);


        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username= String.valueOf(etUsername.getText());
                password= String.valueOf(etPassword.getText());
                Log.e(TAG, password);


                // Retrofit for GET User Info

                // Initialize the Retrofit Interface
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                // Calling Method for Retrofit
                // response : GET User from Accounts
                Call<List<Accounts>> call = apiService.getUserLogin(username, password);
                call.enqueue(new Callback<List<Accounts>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<Accounts>> call, @NonNull Response<List<Accounts>> response) {
                       pDialog.dismiss();
                        /*alert dialog*/
                        alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                        // Parse from body to List<Accounts>
                        account = response.body();

                        Log.i(TAG, "mas"+account);
                        if(username.trim().length()>0 && password.trim().length()>0){
                            if (account.size()!= 0){
                                pDialog.show();
                                String akun= account.get(0).getNama_bisnis();
                                String password= account.get(0).getKata_sandi();
                                int saldo= account.get(0).getSaldo();

                                /*put session*/
                                editor= session.edit();
                                editor.putString("Name", username);
                                editor.putString("Bisnis", akun);
                                editor.putString("Password", password);
                                editor.putInt("Saldo", saldo);
                                editor.putBoolean("logged", true);
                                editor.commit();

                                launchMainScreen();
                            }else{
                                // Setting Dialog Title
                                alertDialog.setTitle("Perhatian!!!");

                                // Setting Dialog Message
                                alertDialog.setMessage("Username atau Password anda salah!");

                                // Showing Alert Message
                                alertDialog.show();//dialog.dismiss();
                                return;
                            }
                        }else{
                            // Setting Dialog Title
                            alertDialog.setTitle("Perhatian!!!");

                            // Setting Dialog Message
                            alertDialog.setMessage("Tolong isi username atau password anda");

                            // Showing Alert Message
                            alertDialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Accounts>> call, Throwable t) {
                        pDialog.dismiss();

                        Log.e(TAG, "onFailure: ", t);

                        // Setting Dialog Title
                        alertDialog.setTitle("Koneksi anda bermasalah");

                        // Showing Alert Message
                        alertDialog.show();
//                        Toast.makeText(LoginActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                    }


                });
            }
        });
    }

    public void onClick(View v){
        launchRegisterScreen();
    }

    private void launchRegisterScreen() {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    private void launchMainScreen(){
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }


}
