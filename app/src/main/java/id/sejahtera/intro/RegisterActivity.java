package id.sejahtera.intro;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText etNamaPanggilan, etNamalengkap, etEmail, etNumber, etPassword, etPasswordConfirmation;
    private CheckBox checkboxEmailPermisiion, checkboxPermission;
    private Button btRegist;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    private String namaPanggilan, namaLengkap, email, number, password, passwordConfirmation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

        /**
        * back on toolbar
        * */
//        just insert image for back button
        myToolbar.setNavigationIcon(R.drawable.back_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchLoginScreen();
            }
        });


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sedang dalam proses...");

        /*initialize editext and checkbox*/
        etNamaPanggilan= findViewById(R.id.etNamaPanggilan);
        etNamalengkap= findViewById(R.id.etNamaLengkap);
        etEmail= findViewById(R.id.etEmail);
        etNumber= findViewById(R.id.etNumber);
        etPassword= findViewById(R.id.etPassword);
        etPasswordConfirmation= findViewById(R.id.etPasswordConfirmation);
        checkboxEmailPermisiion= findViewById(R.id.checkboxEmailPermission);
        checkboxPermission= findViewById(R.id.checkboxPermission);

        /**
         * regist button
         * */
        btRegist= findViewById(R.id.btRegist);

        btRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                namaLengkap= String.valueOf(etNamalengkap.getText());
                namaPanggilan= String.valueOf(etNamaPanggilan.getText());
                email= String.valueOf(etEmail.getText());
                number= String.valueOf(etNumber.getText());
                password= String.valueOf(etPassword.getText());
                passwordConfirmation= String.valueOf(etPasswordConfirmation.getText());

                String bisnis= "";
                alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                if(!checkboxPermission.isChecked()){
                    // Setting Dialog Title
                    alertDialog.setTitle("Perhatian!!!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Tolong baca dan setejui syarat dan ketentuan");

                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    if(!password.equals(passwordConfirmation)){
                        // Setting Dialog Title
                        alertDialog.setTitle("Perhatian!!!");

                        // Setting Dialog Message
                        alertDialog.setMessage("Password yang anda masukan tidak sama");

                        // Showing Alert Message
                        alertDialog.show();
                    }else{
                        if(namaLengkap.trim().length()>0 && namaPanggilan.trim().length()>0
                                && email.trim().length()>0 && number.trim().length()>0
                                && password.trim().length()>0 && passwordConfirmation.trim().length()>0){

                            pDialog.show();

                            // Retrofit for POST User Info

                            // Initialize the Retrofit Interface
                            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                            // Calling Method for Retrofit
                            // response : POST User from Accounts
                            Call<Accounts> call = apiService.postUser(namaPanggilan, namaLengkap,
                                    email, number, password, passwordConfirmation, bisnis);

                            call.enqueue(new Callback<Accounts>() {
                                @Override
                                public void onResponse(@NonNull Call<Accounts> call, @NonNull Response<Accounts> response) {
                                    pDialog.dismiss();
                                    launchLoginScreen();
                                }

                                @Override
                                public void onFailure(Call<Accounts> call, Throwable t) {
                                    pDialog.dismiss();
                                    Log.e("FK", "onFailure: ", t);
                                    // Setting Dialog Title
                                    alertDialog.setTitle("Koneksi anda bermasalah");

                                    // Showing Alert Message
                                    alertDialog.show();
//                                    Toast.makeText(RegisterActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                                }


                            });

                        }else{
                            // Setting Dialog Title
                            alertDialog.setTitle("Perhatian!!!");

                            // Setting Dialog Message
                            alertDialog.setMessage("Tolong lengkapi form terlebih dahulu");

                            // Showing Alert Message
                            alertDialog.show();
                        }
                    }

                }

            }
        });
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        launchLoginScreen();
    }

    private void launchLoginScreen() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }
}
