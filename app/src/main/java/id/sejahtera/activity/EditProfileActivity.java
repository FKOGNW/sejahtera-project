package id.sejahtera.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;

public class EditProfileActivity extends AppCompatActivity {
    private EditText etNamalengkap, etEmail, etNumber, etPassword, etPasswordConfirmation;
    private Button btUpdate;

    private String namaLengkap, email, number, password, passwordConfirmation, tempUsername;

    SharedPreferences session;
    SharedPreferences.Editor editor;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    private List<Accounts> account;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

        /**
         * back on toolbar
         * */
//        just insert image for back button
        myToolbar.setNavigationIcon(R.drawable.back_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMainScreen();
            }
        });


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        /*initialize editext and checkbox*/
        etNamalengkap= findViewById(R.id.etNamaLengkap);
        etEmail= findViewById(R.id.etEmail);
        etNumber= findViewById(R.id.etNumber);
        etPassword= findViewById(R.id.etPassword);
        etPasswordConfirmation= findViewById(R.id.etPasswordConfirmation);

        btUpdate= findViewById(R.id.btUpdate);

        /*session login*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        tempUsername= session.getString("Name", "");
        final String tempPassword= session.getString("Password", "");

        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sedang menyambungkan...");
        pDialog.show();

        // Retrofit for GET User Info

        // Initialize the Retrofit Interface
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        // Calling Method for Retrofit
        // response : GET User from Accounts
        Call<List<Accounts>> call = apiService.getUserProfile(tempUsername, tempPassword);
        call.enqueue(new Callback<List<Accounts>>() {
            @Override
            public void onResponse(@NonNull Call<List<Accounts>> call, @NonNull Response<List<Accounts>> response) {
                pDialog.dismiss();
                account = response.body();
                if (account.size()!= 0){
                    etNamalengkap.setText(account.get(0).getNama_lengkap());
                    etEmail.setText(account.get(0).getEmail());
                    etNumber.setText(account.get(0).getNo_telepon());
                    etPassword.setText(account.get(0).getKata_sandi());
                    etPasswordConfirmation.setText(account.get(0).getKonfirmasi_kata_sandi());
                }
            }

            @Override
            public void onFailure(Call<List<Accounts>> call, Throwable t) {
                Log.e("FK", "onFailure: ", t);
                pDialog.dismiss();
                // Setting Dialog Title
                alertDialog.setTitle("Koneksi anda bermasalah");

                // Showing Alert Message
                alertDialog.show();

                pDialog.dismiss();

                launchMainScreen();
//                Toast.makeText(EditProfileActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
            }


        });



        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                alertDialog = new AlertDialog.Builder(EditProfileActivity.this).create();

                namaLengkap= String.valueOf(etNamalengkap.getText());
                email= String.valueOf(etEmail.getText());
                number= String.valueOf(etNumber.getText());
                password= String.valueOf(etPassword.getText());
                passwordConfirmation= String.valueOf(etPasswordConfirmation.getText());
//                Log.i("FK", passwordConfirmation);

                if(!password.equals(passwordConfirmation)){
                    // Setting Dialog Title
                    alertDialog.setTitle("Perhatian!!!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Password yang anda masukan tidak sama");

                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    if(namaLengkap.trim().length()>0
                            && email.trim().length()>0 && number.trim().length()>0
                            && password.trim().length()>0 && passwordConfirmation.trim().length()>0){
                        pDialog.show();

                        // Retrofit for POST User Info

                        // Initialize the Retrofit Interface
                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                        // Calling Method for Retrofit
                        // response : POST User from Accounts
                        Call<Accounts> call = apiService.putUserProfile(tempUsername,namaLengkap,
                                email, number, password, passwordConfirmation);

                        call.enqueue(new Callback<Accounts>() {
                            @Override
                            public void onResponse(@NonNull Call<Accounts> call, @NonNull Response<Accounts> response) {
                                pDialog.dismiss();
                                editor= session.edit();
                                editor.putString("Password", tempPassword);
                                launchMainScreen();
                            }

                            @Override
                            public void onFailure(Call<Accounts> call, Throwable t) {
                                Log.e("FK", "onFailure: ", t);
                                pDialog.dismiss();
                                // Setting Dialog Title
                                alertDialog.setTitle("Koneksi anda bermasalah");

                                // Showing Alert Message
                                alertDialog.show();
//                                Toast.makeText(EditProfileActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                            }


                        });

                    }else{
                        // Setting Dialog Title
                        alertDialog.setTitle("Perhatian!!!");

                        // Setting Dialog Message
                        alertDialog.setMessage("Tolong lengkapi form terlebih dahulu");

                        // Showing Alert Message
                        alertDialog.show();

                        pDialog.dismiss();
                    }
                }
            }
        });
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        launchMainScreen();
    }

    private void launchMainScreen() {
        startActivity(new Intent(EditProfileActivity.this, MainActivity.class));
        finish();
    }
}
