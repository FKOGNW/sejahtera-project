package id.sejahtera.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Accounts;
import id.sejahtera.model.Business;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;

public class EditBusinessActivity extends AppCompatActivity {

    private EditText etNamaBisnis, etBisnisKategori, etAlamat, etDeskripsi;
    private Button btUpdate;

    private String namaBisnis, bisnisKategori, alamat, deskripsi;

    SharedPreferences session;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    private List<Business> account;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_profile);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

        /**
         * back on toolbar
         * */
//        just insert image for back button
        myToolbar.setNavigationIcon(R.drawable.back_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMainScreen();
            }
        });


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        /*create progress dialog*/
//        pDialog= new ProgressDialog(this);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Sedang menyambungkan...");


        /**
         * session management
         * */
        session = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        /*Initialize editext*/
        etNamaBisnis= findViewById(R.id.etNamaBisnis);
        etBisnisKategori= findViewById(R.id.etBisnisKategori);
        etAlamat= findViewById(R.id.etAlamat);
        etDeskripsi= findViewById(R.id.etDeskripsi);

        /*bt add*/
        btUpdate= findViewById(R.id.btUpdate);

        /*session login*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        final String tempUsername= session.getString("Name", "");
        final String tempPassword= session.getString("Password", "");

        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sedang menyambungkan...");
        pDialog.show();

        // Retrofit for GET User Info

        // Initialize the Retrofit Interface
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        // Calling Method for Retrofit
        // response : GET User from Accounts
        Call<List<Business>> call = apiService.getUserBusiness(tempUsername, tempPassword);
        call.enqueue(new Callback<List<Business>>() {
            @Override
            public void onResponse(@NonNull Call<List<Business>> call, @NonNull Response<List<Business>> response) {
                pDialog.dismiss();
                account = response.body();
                if (account.size()!= 0){
                    etNamaBisnis.setText(account.get(0).getNama_bisnis());
                    etBisnisKategori.setText(account.get(0).getKategori());
                    etAlamat.setText(account.get(0).getAlamat());
                    etDeskripsi.setText(account.get(0).getDeskripsi());
                }
            }

            @Override
            public void onFailure(Call<List<Business>> call, Throwable t) {
                Log.e("FK", "onFailure: ", t);
                pDialog.dismiss();
                // Setting Dialog Title
                alertDialog.setTitle("Koneksi anda bermasalah");

                // Showing Alert Message
                alertDialog.show();

                launchMainScreen();
//                Toast.makeText(EditProfileActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
            }


        });

        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog.show();

                namaBisnis= String.valueOf(etNamaBisnis.getText());
                bisnisKategori= String.valueOf(etBisnisKategori.getText());
                alamat= String.valueOf(etAlamat.getText());
                deskripsi= String.valueOf(etDeskripsi.getText());

                alertDialog = new AlertDialog.Builder(EditBusinessActivity.this).create();

                if(namaBisnis.trim().length()>0 && bisnisKategori.trim().length()>0
                        && alamat.trim().length()>0 && deskripsi.trim().length()>0){

                    // Retrofit for POST User Info

                    // Initialize the Retrofit Interface
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    // Calling Method for Retrofit
                    // response : POST User from Accounts
                    Call<Business> call = apiService.putBusiness(session.getString("Name", ""),namaBisnis, bisnisKategori, alamat, deskripsi);
                    Log.i("FK", deskripsi);
                    call.enqueue(new Callback<Business>() {
                        @Override
                        public void onResponse(@NonNull Call<Business> call, @NonNull Response<Business> response) {
                            pDialog.dismiss();
                            launchMainScreen();
                        }

                        @Override
                        public void onFailure(Call<Business> call, Throwable t) {
                            Log.e("FK", "onFailure: ", t);

                            pDialog.dismiss();

                            // Setting Dialog Title
                            alertDialog.setTitle("Koneksi anda bermasalah");

                            // Showing Alert Message
                            alertDialog.show();

                            pDialog.dismiss();
//                            Toast.makeText(AddBusinessActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                        }


                    });

                }else{
                    // Setting Dialog Title
                    alertDialog.setTitle("Perhatian!!!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Tolong lengkapi form terlebih dahulu");

                    // Showing Alert Message
                    alertDialog.show();

                    pDialog.dismiss();
                }

            }
        });
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        launchMainScreen();
    }

    private void launchMainScreen() {
        startActivity(new Intent(EditBusinessActivity.this, MainActivity.class));
        finish();
    }
}
