
package id.sejahtera.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Date;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.fragment.ForumFragment;
import id.sejahtera.model.Business;
import id.sejahtera.model.Posting;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;

public class PostActivity extends AppCompatActivity {

    Button btPost;
    private EditText etJudul, etIsi;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

        /**
         * back on toolbar
         * */
//        just insert image for back button
        myToolbar.setNavigationIcon(R.drawable.back_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchFragmentScreen();
            }
        });


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        session = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sedang menyambungkan...");btPost= findViewById(R.id.btPost);

        etJudul= findViewById(R.id.etJudul);
        etIsi= findViewById(R.id.etIsi);

        btPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String judul= String.valueOf(etJudul.getText());
                String isi= String.valueOf(etIsi.getText());

                String date = DateFormat.getDateTimeInstance().format(new Date());

                Log.i("FK", "aa"+date);

                alertDialog = new AlertDialog.Builder(PostActivity.this).create();

                if(judul.trim().length()>0 && isi.trim().length()>0){
                    pDialog.show();

                    // Retrofit for POST User Info

                    // Initialize the Retrofit Interface
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    // Calling Method for Retrofit
                    // response : POST User from Accounts
                    Call<Posting> call = apiService.post(session.getString("Name", ""), judul, isi, date);
                    call.enqueue(new Callback<Posting>() {
                        @Override
                        public void onResponse(@NonNull Call<Posting> call, @NonNull Response<Posting> response) {
                            pDialog.dismiss();
                            launchFragmentScreen();
                        }

                        @Override
                        public void onFailure(Call<Posting> call, Throwable t) {
                            Log.e("FK", "onFailure: ", t);

                            pDialog.dismiss();

                            // Setting Dialog Title
                            alertDialog.setTitle("Koneksi anda bermasalah");

                            // Showing Alert Message
                            alertDialog.show();
//                            Toast.makeText(AddBusinessActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                        }


                    });
                }else{
                    // Setting Dialog Title
                    alertDialog.setTitle("Perhatian!!!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Tolong lengkapi form terlebih dahulu");

                    // Showing Alert Message
                    alertDialog.show();
                }
            }
        });
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        launchFragmentScreen();
    }

    private void launchFragmentScreen() {

        startActivity(new Intent(PostActivity.this, MainActivity.class));
        finish();

    }
}
