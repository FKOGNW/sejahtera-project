package id.sejahtera.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.List;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.fragment.DompetFragment;
import id.sejahtera.fragment.ForumFragment;
import id.sejahtera.fragment.NewsFragment;
import id.sejahtera.fragment.PencarianFragment;
import id.sejahtera.intro.LoginActivity;
import id.sejahtera.model.Accounts;
import id.sejahtera.model.Payment;
import id.sejahtera.model.Transaksi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences session;
    SharedPreferences.Editor editor;

    private String tempBisnis, tempName, tempStatus, tempAlamat;

    AlertDialog alertDialog;
    ProgressDialog pDialog;

    NavigationView navigationView;
    View headerView;

    private List<Accounts> account;

    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);



        /**
         * side nav*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, myToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);

        /*cretae alert dialog*/
        alertDialog= new AlertDialog.Builder(MainActivity.this).create();

//        /*craete progres bar*/
//        pDialog= new ProgressDialog(MainActivity.this);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Sedang menyambungkan...");
//        pDialog.show();

        /**
         * session management
         * */
        session = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = session.edit();

        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container2);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primaryColor,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

/**
 * Showing Swipe Refresh animation on activity create
 * As animation won't start on onCreate, post runnable is used
 */
        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                loadDataMain();
            }
        });


//        bottom naviagtion
        BottomNavigationViewEx bottomNavigationView = (BottomNavigationViewEx)findViewById(R.id.navigation);
        bottomNavigationView.enableAnimation(true);
        bottomNavigationView.enableShiftingMode(false);
        bottomNavigationView.enableItemShiftingMode(false);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.btDompet:
//                                Toast.makeText(MainActivity.this, "DompetKu", Toast.LENGTH_SHORT).show();
                                selectedFragment = new DompetFragment();
                                break;
                            case R.id.btSeputar:
//                                Toast.makeText(MainActivity.this, "Seputar UMKM", Toast.LENGTH_SHORT).show();
                                selectedFragment = new NewsFragment();
                                break;
//                            case R.id.btPencarian:
////                                Toast.makeText(MainActivity.this, "Pencarian", Toast.LENGTH_SHORT).show();
//                                selectedFragment = new PencarianFragment();
//                                break;
                            case R.id.btForum:
//                                Toast.makeText(MainActivity.this, "Forum", Toast.LENGTH_SHORT).show();
                                selectedFragment = new ForumFragment();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });



    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.navPersonal) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        displaySelectedFragment(item.getItemId());
        return true;
    }

    private void displaySelectedFragment(int menuItemId) {
        android.support.v4.app.Fragment fragment = null;
        switch (menuItemId) {
            case R.id.navAjukanPinjaman:
                if(tempBisnis.equals("")){
                    AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Bisnis anda belum terdaftar");

                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    if (!session.getString("Status", "").equals("")){
                        AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);

                        // Setting Dialog Title
                        alertDialog.setTitle("Pinjaman anda belum lunas");

                        // Showing Alert Message
                        alertDialog.show();
                    }else{
                        startActivity(new Intent(MainActivity.this, AddSaldoActivity.class));
                    }
                }
                break;
            case R.id.navBayarPinjaman:
                if(tempBisnis.equals("")){
                    AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Anda belum mendaftarkan bisnis anda!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Ingin mendaftarkan bisnis anda?");

                    alertDialog.setPositiveButton("YA",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    startActivity(new Intent(MainActivity.this, AddBusinessActivity.class));
                                }
                            });

                    alertDialog.setNegativeButton("TIDAK",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });


                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    if(tempStatus.equals("Terverifikasi")) {
                        AlertDialog.Builder alertDialog1 = new AlertDialog.Builder(this);

                        // Setting Dialog Message
                        alertDialog1.setMessage("Anda yakin membayar cicilan sebesar Rp. " + session.getInt("Cicilan", 0) + " ?");

                        alertDialog1.setPositiveButton("YA",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        int tempBelumBayar = session.getInt("Belum bayar", 0);
                                        int tempSudahBayar = session.getInt("Sudah bayar", 0);
                                        int cicilan = session.getInt("Cicilan", 0);

                                        int finalBelumBayar = tempBelumBayar - cicilan;
                                        int finalSudahBayar = tempSudahBayar + cicilan;

                                        pDialog.show();
                                        // Initialize the Retrofit Interface
                                        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                                        // Calling Method for Retrofit
                                        // response : POST User from Accounts
                                        Call<Payment> call = apiService.putPayment(session.getString("Name", ""),
                                                finalSudahBayar, finalBelumBayar);
//                Log.i("FK", deskripsi);
                                        call.enqueue(new Callback<Payment>() {
                                            @Override
                                            public void onResponse(@NonNull Call<Payment> call, @NonNull Response<Payment> response) {
                                                pDialog.dismiss();
                                                launchMainScreen();
                                            }

                                            @Override
                                            public void onFailure(Call<Payment> call, Throwable t) {
                                                Log.e("FK", "onFailure: ", t);

                                                pDialog.dismiss();

                                                // Setting Dialog Title
                                                alertDialog.setTitle("Koneksi anda bermasalah");

                                                // Showing Alert Message
                                                alertDialog.show();

                                                pDialog.dismiss();
//                            Toast.makeText(AddBusinessActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                                            }


                                        });
                                    }
                                });

                        alertDialog1.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        // Showing Alert Message
                        alertDialog1.show();
                    }else{
                        AlertDialog.Builder alertDialog2= new AlertDialog.Builder(this);

                        // Setting Dialog Title
                        alertDialog2.setTitle("Peminjaman anda belum terverifikasi");

                        // Showing Alert Message
                        alertDialog2.show();
                    }

                }

                break;
            case R.id.navPersonal:
                startActivity(new Intent(MainActivity.this, EditProfileActivity.class));
                break;
            case R.id.navBisnis:
                if(tempBisnis.equals("")){
                    AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Anda belum mendaftarkan bisnis anda!");

                    // Setting Dialog Message
                    alertDialog.setMessage("Ingin mendaftarkan bisnis anda?");

                    alertDialog.setPositiveButton("YA",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    startActivity(new Intent(MainActivity.this, AddBusinessActivity.class));
                                }
                            });

                    alertDialog.setNegativeButton("TIDAK",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });


                    // Showing Alert Message
                    alertDialog.show();
                }else{
                    startActivity(new Intent(MainActivity.this, EditBusinessActivity.class));
                }
                break;
            case R.id.navPengaturan:
//                fragment = new SettingsFragment();
                break;
            case R.id.navBantuan:
                break;
            case R.id.navKeluar:
                editor.clear();
                editor.commit();
                finish();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                break;
            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void launchMainScreen() {
        startActivity(new Intent(MainActivity.this, MainActivity.class));

    }

    @Override
    public void onRefresh() {
        loadDataMain();
    }

    public void loadDataMain(){

        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);

        // Retrofit for GET User Info

        // Initialize the Retrofit Interface
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        // Calling Method for Retrofit
        // response : GET User from Accounts
        Call<List<Accounts>> call = apiService.getUserProfile(session.getString("Name", ""),
                session.getString("Password", ""));
        call.enqueue(new Callback<List<Accounts>>() {
            @Override
            public void onResponse(@NonNull Call<List<Accounts>> call, @NonNull Response<List<Accounts>> response) {
                account = response.body();
                if (account.size()!= 0){
                    editor.putString("Password", account.get(0).getKata_sandi());
                    editor.putString("Bisnis", account.get(0).getNama_bisnis());
                    editor.putInt("Saldo", account.get(0).getSaldo());
                    editor.putString("Status", account.get(0).getStatus_pinjaman());
                    editor.putInt("Sudah bayar", account.get(0).getNominal_sudah_bayar());
                    editor.putInt("Belum bayar", account.get(0).getNominal_belum_bayar());
                    editor.putInt("Cicilan", account.get(0).getCicilan());
                    editor.putString("Alamat", account.get(0).getAlamat());
                    editor.commit();


//                    pDialog.dismiss();

                    /*set name text*/
                    TextView etName= headerView.findViewById(R.id.tvName);
                    TextView tvBisnis= headerView.findViewById(R.id.tvBisnis);

                    tempBisnis= session.getString("Bisnis", "");
                    tempName= session.getString("Name", "");
                    tempStatus= session.getString("Status", "");
                    tempAlamat= session.getString("Alamat", "");



                    if (tempBisnis.trim().length()>0){
                        tvBisnis.setText(tempBisnis);
                    }else{
                        tvBisnis.setText("Bisnis anda belum terdaftar");
                    }
                    etName.setText(tempName);
//Manually displaying the first fragment - one time only
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, new DompetFragment());
                    transaction.commit();

// Stopping swipe refresh
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.i("FK", String.valueOf(account.get(0).getNominal_belum_bayar()));
                }
            }

            @Override
            public void onFailure(Call<List<Accounts>> call, Throwable t) {
                Log.e("FK", "onFailure: ", t);

//                pDialog.dismiss();

                // Setting Dialog Title
                alertDialog.setTitle("Koneksi anda bermasalah");

                // Showing Alert Message
                alertDialog.show();

                // Stopping swipe refresh
                mSwipeRefreshLayout.setRefreshing(false);
//                Toast.makeText(MainActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
            }


        });



    }
}
