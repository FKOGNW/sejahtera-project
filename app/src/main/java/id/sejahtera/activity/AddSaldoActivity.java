package id.sejahtera.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.List;

import id.sejahtera.R;
import id.sejahtera.api.ApiClient;
import id.sejahtera.api.ApiInterface;
import id.sejahtera.model.Business;
import id.sejahtera.model.Transaksi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.sejahtera.activity.MainActivity.MyPREFERENCES;

public class AddSaldoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private TextView tvSaldo, tvBunga, tvPinjaman, tvCicilan;

    int saldo, pinjaman, cicilan, belumBayar;

    SharedPreferences session;
    SharedPreferences.Editor editor;

    Button btAjukan;

    AlertDialog alertDialog;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_saldo);


        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // making notification bar transparent
        changeStatusBarColor();

        // Retrieve the AppCompact Toolbar
        Toolbar myToolbar = findViewById(R.id.toolbar2);
        setSupportActionBar(myToolbar);

        /*make toolbar without title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        myToolbar.setTitle("");
        myToolbar.setSubtitle("");

        /**
         * back on toolbar
         * */
//        just insert image for back button
        myToolbar.setNavigationIcon(R.drawable.back_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);


        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMainScreen();
            }
        });


        // Set the padding to match the Status Bar height
        myToolbar.setPadding(0, getStatusBarHeight(), 0, 0);

        /**
         * Spinner for saldo*/
        // Spinner element
        Spinner spinner = findViewById(R.id.spinnerNominal);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);


        String[] ITEMS = {"Rp.2.500.000", "Rp.5.000.000", "Rp.7.500.000", "Rp.10.000.000"};
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
/**
 * session management
 * */
        session = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        final String username= session.getString("Name", "");

        btAjukan= findViewById(R.id.btAjukan);

        /*create progress dialog*/
        pDialog= new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Sedang menyambungkan...");

        btAjukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pDialog.show();
                // Initialize the Retrofit Interface
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                // Calling Method for Retrofit
                // response : POST User from Accounts
                Call<Transaksi> call = apiService.putTransaksi(username, saldo, "Sedang mengajukan", pinjaman
                        , cicilan, belumBayar);
//                Log.i("FK", deskripsi);
                call.enqueue(new Callback<Transaksi>() {
                    @Override
                    public void onResponse(@NonNull Call<Transaksi> call, @NonNull Response<Transaksi> response) {
                        pDialog.dismiss();
                        launchMainScreen();
                    }

                    @Override
                    public void onFailure(Call<Transaksi> call, Throwable t) {
                        Log.e("FK", "onFailure: ", t);

                        pDialog.dismiss();

                        // Setting Dialog Title
                        alertDialog.setTitle("Koneksi anda bermasalah");

                        // Showing Alert Message
                        alertDialog.show();

                        pDialog.dismiss();
//                            Toast.makeText(AddBusinessActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                    }


                });
            }
        });
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        launchMainScreen();
    }

    private void launchMainScreen() {
        startActivity(new Intent(AddSaldoActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item= parent.getItemAtPosition(position).toString();

        tvSaldo= findViewById(R.id.tvSaldo);
        tvBunga= findViewById(R.id.tvBunga);
        tvPinjaman= findViewById(R.id.tvPinjaman);
        tvCicilan= findViewById(R.id.tvCicilan);

        tvSaldo.setText(item);


//        double tempSaldo = 0, itungBunga= 0;
//        float besarBunga= 5/100;
        if(item.equals("Rp.2.500.000")){
//            tempSaldo= 2500000;
//            tvBunga.setText("Rp.125.000");
            tvPinjaman.setText("Rp.2.650.000");
            tvCicilan.setText("Rp.110.416 /Bulan");

            saldo= 2500000;
            pinjaman= 2650000;
            cicilan= 110416;
            belumBayar= 2650000;
        }else if(item.equals("Rp.5.000.000")){
//            tvBunga.setText("Rp.250.000");
            tvPinjaman.setText("Rp.5.150.000");
            tvCicilan.setText("Rp.214.583 /Bulan");

            saldo= 5000000;
            pinjaman= 5150000;
            cicilan= 214583;
            belumBayar= 5150000;
        }else if(item.equals("Rp.7.500.000")){
//            tvBunga.setText("Rp.375.000");
            tvPinjaman.setText("Rp.7.650.000");
            tvCicilan.setText("Rp.318.750 /Bulan");

            saldo= 7500000;
            pinjaman= 7650000;
            cicilan= 318750;
            belumBayar= 7650000;
        }else if(item.equals("Rp.10.000.000")){
//            tvBunga.setText("Rp.500.0000");
            tvPinjaman.setText("Rp.10.150.000");
            tvCicilan.setText("Rp.422.916 /Bulan");

            saldo= 10000000;
            pinjaman= 10150000;
            cicilan= 422916;
            belumBayar= 10150000;
        }
//        itungBunga= tempSaldo*0.05;
//        int saldo= tempSaldo;

//
//        int bunga= tempSaldo*(tempSaldo/100);

//        tvBunga.setText(bunga);

//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
